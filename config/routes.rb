Rails.application.routes.draw do
  resources :projects
  get 'site/home'

  devise_for :admins
  devise_for :users
  root 'site#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
