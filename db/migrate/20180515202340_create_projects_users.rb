class CreateProjectsUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :projects_users do |t|
      t.references :Project, foreign_key: true
      t.references :User, foreign_key: true

      t.timestamps
    end
  end
end
